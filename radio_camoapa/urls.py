# -*- coding: utf-8 -*-
"""radio_camoapa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from radio.feeds import LatestArticulosFeed
from django.views.generic import TemplateView
from parrila_programacion.views import *

admin.site.site_header = "Administración Radio Camoapa"
admin.site.site_title = "Administración Radio Camoapa"

urlpatterns = [
    url(r'', include('radio.urls')),
    #url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^rss/$', LatestArticulosFeed()),
    url(r'^busqueda/', include('django_google_cse.urls')),
    url(r'^ads\.txt$', TemplateView.as_view(template_name='ads.txt')),
    url(r'^programacion', programacion, name='programacion'),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns += staticfiles_urlpatterns()
