from django import template
import locale
import os
register = template.Library()

@register.filter(name='file_extencion')
def get_file_extencion(value):
    return os.path.splitext(value.path)[1]
