from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^noticias/$', views.noticias_por_categoria, name='noticias'),
     url(r'^lista-noticias/(?P<slug>[\w-]+)/$', views.noticias_locales, name='lista-noticia'),
     url(r'^producciones/(?P<slug>[\w-]+)/$', views.produccion_locales, name='lista-produccion'),
    url(r'^noticias/(?P<subcategoria>[\w-]+)/(?P<slug>[\w-]+)/$', views.DetailNoticias, name='noticia-detalle'),
    url(r'^contacto/$', views.contacto, name='contacto'),
    url(r'^galerias/(?P<slug>[\w-]+)/$', views.GaleriaDetailView.as_view(), name='galeria-detalle'),
    url(r'^producciones/$', views.lista_producciones, name='producciones'),
    url(r'^producciones/(?P<subcategoria>[\w-]+)/(?P<slug>[\w-]+)/$', views.DetailProduccion, name='produccion-detalle'),
]
