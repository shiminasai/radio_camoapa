# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from sorl.thumbnail import ImageField

# Create your models here.
DIA_CHOICES = ((1,'Domingo'),(2,'Lunes'),(3,'Martes'),(4,'Miércoles'),
                (5,'Jueves'),(6,'Viernes'),(7,'Sábado'))

class Programacion(models.Model):
    dia = models.IntegerField(choices=DIA_CHOICES)

    def __str__(self):
        return self.get_dia_display().encode('utf-8')

    class Meta:
        verbose_name_plural = 'Programaciones'

class ProgramacionDia(models.Model):
    programacion = models.ForeignKey(Programacion)
    titulo = models.CharField(max_length=250)
    descripcion = models.TextField()
    hora_inicio = models.TimeField()
    hora_fin = models.TimeField(blank=True, null=True)
    imagen = ImageField(upload_to='programacion/',blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Programacion del dia'
