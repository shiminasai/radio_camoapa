from __future__ import unicode_literals

from django.apps import AppConfig


class ParrilaProgramacionConfig(AppConfig):
    name = 'parrila_programacion'
