from django.contrib import admin
from .models import *
from django.forms import Textarea
# Register your models here.

class ProgramacionDia_Inline(admin.TabularInline):
    model = ProgramacionDia
    extra = 1
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows':1, 'cols':40,'style': 'height: 100px;resize:none;',})},
	}

class ProgramacionAdmin(admin.ModelAdmin):
    inlines = [ProgramacionDia_Inline,]

admin.site.register(Programacion,ProgramacionAdmin)
